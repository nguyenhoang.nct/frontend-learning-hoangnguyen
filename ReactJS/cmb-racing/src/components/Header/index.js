import React, { useState } from "react";
import {
  Collapse,
  Navbar,
  NavbarToggler,
  NavbarBrand,
  Nav,
  NavItem,
  NavLink as BSNavLink,
  UncontrolledDropdown,
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  Container
} from "reactstrap";
import styled from "styled-components";
import { Link, NavLink, useLocation, Redirect } from "react-router-dom";

import Banner from "../Banner";
import Logo from "../../images/logo.png";
import AboutBanner from "../../images/banner/about.png";

const StyledBrand = styled(Link)`
  width: 260px;
  > img {
    width: 100%;
  }
`;

const StyledNavbar = styled(Navbar)`
  position: fixed;
  top: 60px;
  z-index: 10;
  padding: 0;
  width: 1140px;
`;

const StyledNavLink = styled(Link)`
  color: #fff;
  font-size: 16px;
  padding: 0;
  margin-left: 40px;
  &:hover, &.active {
    color: #ff6600 !important;
  }
`;

const StyledDropdownToggle = styled(DropdownToggle)`
  color: #fff !important;
  font-size: 16px;
  padding: 0;
  margin-left: 40px;
  &:hover {
    color: #ff6600 !important;
  }
`;

const StyledCollapse = styled(Collapse)`
  justify-content: flex-end;
`;

const Header = ({isHome}) => {
  const [isOpen, setIsOpen] = useState(false);

  const toggle = () => setIsOpen(!isOpen);
  const location = useLocation();

  return (
    <div>
      <Banner isHome={isHome} title="" height="100vh" img={AboutBanner}/>
      <Container>
        <StyledNavbar light expand="md">
          <StyledBrand to="/"><img src={Logo} /></StyledBrand>
          <NavbarToggler onClick={toggle} />
          <StyledCollapse isOpen={isOpen} navbar>
            <Nav navbar>
              <NavItem>
                <StyledNavLink 
                  to="/about"
                  className={location.pathname === "/about" ? "active" : ""}
                >
                  About
                </StyledNavLink>
              </NavItem>
              <UncontrolledDropdown nav inNavbar>
                <StyledDropdownToggle nav caret>
                  Stable
                </StyledDropdownToggle>
                <DropdownMenu right>
                  <DropdownItem>
                    <NavLink 
                      to="/stable1" 
                      className={location.pathname === "/stable1" ? "active" : ""}
                    >
                      Stable 1
                    </NavLink>
                  </DropdownItem>
                  <DropdownItem>
                    <NavLink 
                      to="/stable2"  
                      className={location.pathname === "/stable2" ? "active" : ""}
                    >
                      Stable 2
                    </NavLink>
                  </DropdownItem>
                </DropdownMenu>
              </UncontrolledDropdown>
              <NavItem>
                <StyledNavLink 
                  to="/success-highlight" 
                  className={location.pathname === "/success-highlight" ? "active" : ""}
                >Success & Highlights</StyledNavLink>
              </NavItem>
              <NavItem>
                <StyledNavLink 
                  to="/runner-result"
                  className={location.pathname === "/runner-result" ? "active" : ""}
                >Runners & Results</StyledNavLink>
              </NavItem>
              <NavItem>
                <StyledNavLink 
                  to="/news"
                  className={location.pathname === "/news" ? "active" : ""}
                >News</StyledNavLink>
                {/* <NavLink 
                  to="/news"
                  exact
                  
                >
                  News
                </NavLink> */}
              </NavItem>
              <NavItem>
                <StyledNavLink 
                  to="/contact" 
                  className={location.pathname === "/contact" ? "active" : ""}
                >Contact</StyledNavLink>
              </NavItem>
            </Nav>
          </StyledCollapse>
        </StyledNavbar>
      </Container>
    </div>
  );
}

export default Header;