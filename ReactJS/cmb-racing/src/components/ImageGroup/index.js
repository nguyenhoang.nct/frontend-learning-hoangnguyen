import React, { Component } from 'react';
import { Container } from 'reactstrap';
import styled from "styled-components";

const Wrapper = styled.div`
    width: 100%;
`;
const TopImg = styled.div`
    width: 100%;
    display: flex;
    justify-content: space-between;
`;
const LeftImg = styled.div`
    width: calc(50% - 15px);
    background-color: #ff6600;
    color: #fff;
    font-weight: bold;
    text-align: center;
    text-transform: uppercase;
    border-radius: 10px;
    padding-top: 65px;
    font-size: 26px;
`;

const RightImg = styled.div`
    width: calc(50% - 15px);
    border-radius: 10px;
    height: 265px;
    > img {
        width: 100%;
        height: 100%;
        object-fit: cover;
        border-radius: 10px;
    }
`;

const BottomImg = styled.div`
    width: 100%;
    height: 160px;
    margin-top: 15px;
    border-radius: 10px;
    > img {
        height: 100%;
        width: 100%;
        object-fit: cover;
        border-radius: 10px;
    }
`;

function ImageGroup({title, img1, img2}) {
    return (
        <Wrapper>
            <TopImg>
                <LeftImg>
                    {title}
                </LeftImg>
                <RightImg>
                    <img src={img1} />
                </RightImg>
            </TopImg>
            <BottomImg>
                <img src={img2} />
            </BottomImg>
        </ Wrapper>
    );
}
export default ImageGroup;