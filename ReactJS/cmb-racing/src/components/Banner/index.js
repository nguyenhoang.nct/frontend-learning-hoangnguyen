import React, { useState } from 'react';
import {
    Container,
    Carousel,
    CarouselItem,
    CarouselIndicators,
    CarouselCaption
} from 'reactstrap';
import styled from "styled-components";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretRight } from "@fortawesome/free-solid-svg-icons";

import Img1 from "../../images/banner/home.png";
import Img2 from "../../images/banner/home.png";
import Img3 from "../../images/banner/home.png";
import Img4 from "../../images/banner/home.png";

const Wrapper = styled.div`
    width: 100%;
    background-size: cover;
    background-repeat: no-repeat;
    background-position: center;
    position: relative;
`;

const items = [
    {
        src: Img1,
        title: 'Mazurek makes it a hatrick of wins 1',
        content: '5 year old mare Mazurek made it three out of three this prep where'
    },
    {
        src: Img2,
        title: 'Mazurek makes it a hatrick of wins 2',
        content: '5 year old mare Mazurek made it three out of three this prep where'
    },
    {
        src: Img3,
        title: 'Mazurek makes it a hatrick of wins 3',
        content: '5 year old mare Mazurek made it three out of three this prep where'
    },
    {
        src: Img4,
        title: 'Mazurek makes it a hatrick of wins 4',
        content: '5 year old mare Mazurek made it three out of three this prep where'
    }
];

const Title = styled.div`
    position: absolute;
    top: 50%;
    left: 50%;
    transform: translate(-50%, -50%);
    background-color: rgba(169, 79, 21, 0.6);
    color: #fff;
    text-transform: uppercase;
    font-weight: 600;
    font-size: 40px;
    padding: 10px 30px;
`;

const StyledCarouselItem = styled(CarouselItem)`
    width: 100%;
    height: 100vh;
    overflow: hidden;
    img {
        width: 100%;
        object-fit: cover;
        height: 100vh;
    }
`;

const StyledCaption = styled.div`
    position: absolute;
    top: 50%;
    text-align: left;
    h4 {
        font-size: 40px;
        color: #fff;
        margin-bottom: 20px;
        width: 325px;
    }
    p {
        font-size: 16px;
        color: #888888;
        margin-bottom: 40px;
    }

    a {
        padding: 15px 25px;
        color: #fff !important;
        margin: 0;
        font-size: 16px;
        border-radius: 999px;
        background-color: #ff6600;
        cursor: pointer;
        line-height: 1;
        &:hover {
            opacity: 0.7;
            color: #fff !important;
        }
        svg {
            margin-left: 30px;
        }
    }
`;

const StyledCarouselIndicators = styled(CarouselIndicators)`
    li {
        width: 12px;
        height: 12px;
        background-color: #fff;
        opacity: 1;
        border-radius: 50%;
        margin: 0 6px;
        &.active {
            background-color: #ff6600;
        }
    }
`;

function Banner({title, img, isHome, height}) {
    const [activeIndex, setActiveIndex] = useState(0);
    const [animating, setAnimating] = useState(false);

    const next = () => {
        if (animating) return;
        const nextIndex = activeIndex === items.length - 1 ? 0 : activeIndex + 1;
        setActiveIndex(nextIndex);
    };

    const previous = () => {
        if (animating) return;
        const nextIndex = activeIndex === 0 ? items.length - 1 : activeIndex - 1;
        setActiveIndex(nextIndex);
    };

    const goToIndex = (newIndex) => {
        if (animating) return;
        setActiveIndex(newIndex);
    }

    const slides = items.map((item, index) => {
        return (
            <StyledCarouselItem
                onExiting={() => setAnimating(true)}
                onExited={() => setAnimating(false)}
                key={index}
            >
                <img src={item.src} />
                <Container>
                    <StyledCaption>
                        <h4>{item.title}</h4>
                        <p>{item.content}</p>
                        <a>Read more <FontAwesomeIcon icon={faCaretRight} /></a>
                    </StyledCaption>
                </Container>
            </StyledCarouselItem>
        );
    });

    return (
        <>
            {!isHome &&  
                <Wrapper style={{backgroundImage: `url(${img})`, height: height}}>
                    <Title>{title}</Title>
                </Wrapper>
            }   
            {isHome && 
                <Carousel
                    activeIndex={activeIndex}
                    next={next}
                    previous={previous}
                >
                    <StyledCarouselIndicators items={items} activeIndex={activeIndex} onClickHandler={goToIndex} />
                    {slides}
                </Carousel>
            }
        </>
    );
}

export default Banner;