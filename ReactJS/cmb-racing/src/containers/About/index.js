import React from "react";
import { BrowserRouter, Route, useLocation, useHistory, useRouteMatch, Prompt } from "react-router-dom";

function About() {
    return (
        <div>
            Component About
            <Prompt when={true} message="Are you sure leave about page?" />
        </div>
    )
}

export default About;