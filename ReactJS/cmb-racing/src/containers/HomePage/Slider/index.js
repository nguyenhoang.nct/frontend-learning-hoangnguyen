import React, { Component } from "react";
import Slider from "react-slick";
import styled from "styled-components";
import {Container} from "reactstrap";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faCaretRight} from "@fortawesome/free-solid-svg-icons";

import Bg from "../../../images/slide/bg-horse-available.png";
import Img1 from "../../../images/slide/1.png";
import Img2 from "../../../images/slide/2.png";
import Img3 from "../../../images/slide/3.png";

const Wrapper = styled.div`
  width: 100%;
  background-image: url(${Bg});
  background-repeat: no-repeat;
  background-size: cover;
  background-position: center;
`;

const SlideItem = styled.div`
  width: 100%;
  padding: 0 20px;
  border-radius: 10px;
  img {
    border-top-left-radius: 10px;
    border-top-right-radius: 10px;
    width: 100%;
    height: 225px;
    object-fit: cover;
  }
`;

const SlideInfo = styled.div`
  background-color: #fff;
  text-align: left;
  padding: 30px 35px 35px;
  h4 {
    font-size: 20px;
    color: #ff6600;
  }
  p {
    font-size: 15px;
    color: #737373;
    margin-bottom: 30px;
  }
  >div {
    display: flex;
    flex-direction: column;
    margin-bottom: 30px;
    >div {
      display: flex;
      >div {
        text-align: left;
        &:first-child {
          width: 40%;
        }
        &:last-child {
          width: 60%;
        }
      }
    }
  }
  a {
    display: inline-block;
    padding: 15px 12.5px 15px 25px;
    background-color: #ff6600;
    font-size: 16px;
    color: #fff!important;
    border-radius: 999px;
    svg {
      margin-left: 27.5px;
    }
    &:hover {
      opacity: 0.7;
    }
  }
`;

const horseAvailable = [
  {
    img: Img1,
    horseName: "Belzelady",
    year: "4 years old",
    sire: "Swift Allicen",
    dam: "ComzeTita",
    gender: "Racehose",
    dob: "Oct 20th, 2013"
  },
  {
    img: Img2,
    horseName: "Belzelady",
    year: "4 years old",
    sire: "Swift Allicen",
    dam: "ComzeTita",
    gender: "Racehose",
    dob: "Oct 20th, 2013"
  },
  {
    img: Img3,
    horseName: "Belzelady",
    year: "4 years old",
    sire: "Swift Allicen",
    dam: "ComzeTita",
    gender: "Racehose",
    dob: "Oct 20th, 2013"
  },
  {
    img: Img2,
    horseName: "Belzelady",
    year: "4 years old",
    sire: "Swift Allicen",
    dam: "ComzeTita",
    gender: "Racehose",
    dob: "Oct 20th, 2013"
  },
];

const slide = horseAvailable.map(item => 
  <SlideItem>
    <img src={item.img} />
    <SlideInfo>
      <h4>{item.horseName}</h4>
      <p>{item.year}</p>
      <div>
        <div>
          <div>Sire</div><div>{item.sire}</div>
        </div>
        <div>
          <div>Dam</div><div>{item.dam}</div>
        </div>
        <div>
          <div>Gender</div><div>{item.gender}</div>
        </div>
        <div>
          <div>DOB</div><div>{item.dob}</div>
        </div>
      </div>
      <a>View detail <FontAwesomeIcon icon={faCaretRight}/></a>
    </SlideInfo>
  </SlideItem>
);

function Slide() {
  const settings = {
    dots: true,
    infinite: true,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 1
  };
  return (
    <Wrapper>
      <Container>
        <h2> Multiple items </h2>
        <Slider {...settings}>
          {slide}
        </Slider>
      </Container>
    </Wrapper>
  );
}

export default Slide;