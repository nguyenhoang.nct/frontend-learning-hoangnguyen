import React, { Component, useEffect } from 'react';
import { useLocation, Redirect } from "react-router-dom";
import Welcome from './Welcome';
import Slide from "./Slider";
import Header from "../../components/Header";
import Img1 from "../../images/imagegroup/1.png";
import Img2 from "../../images/imagegroup/2.png";

function HomePage() {
    const location = useLocation();
    if(location) 
        // return <Redirect to="/contact" />;

    return (
        <>
            <Welcome title="Welcome" img1={Img1} img2={Img2} />
            <Slide/>
        </>
    );
}

export default HomePage;