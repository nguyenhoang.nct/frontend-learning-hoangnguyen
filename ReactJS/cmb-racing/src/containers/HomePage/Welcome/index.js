import React from 'react';
import ImageGroup from "../../../components/ImageGroup";
import { Row, Col, Container } from 'reactstrap';
import Info from "./Info";

function Welcome({title, img1, img2}) {
  return (
    <section style={{marginTop:"45px"}}>
      <Container>
        <Row>
        <Col lg="4">
            <ImageGroup title="Welcome" img1={img1} img2={img2} />
        </Col>
        <Col lg="8">
            <Info />
        </Col>
        </Row>
      </Container>
    </section>
  );
}

export default Welcome;
