import React, { Component } from 'react';
import styled from "styled-components";

const Title = styled.h2`
  font-size: 21px;
  font-weight: bold;
  color: #ff6600;
  text-transform: uppercase;
`;

const H4=styled.h4`
  font-size: 16px;
  font-weight: bold;
  margin-top: 25px;
`;

const P=styled.p`
  font-size: 16px;
  color: #5a5959;
  margin-top: 25px;
  margin-bottom: 0;
`;

function Info() {
  return (
    <div style={{textAlign: "left"}}>
      <Title>Chris Bieg Racing</Title>
      <H4>The perfect set up for a boutique team of gallopers!</H4>
      <P>At Chris Bieg Racing every horse is treated as an individual in regards to both it's training and dietary  requirements - the happier the horse the better it will perform.</P>
      <P>Chris' training methods are very hands on, and he believes varying routines is important to keep the horses interested.</P>
      <P>Recent success on the track includes quality galloper, Excites Zelady, a Group 3 and dual Listed winner, with more than $400,000 in prize money under his belt. Other top performing horses include very consistent mare Ample On Offa, exciting 2 year old With A Bit Of Dash, Insuperable and a number of young up-and-coming 2/3 year olds.</P>
      <P>The team at Chris Bieg Racing offer professionalism, honesty and direct communication with owners. It is a family friendly environment, children and family members are encouraged to be apart of the fun, both at the races, stable visits and events.</P>
      <P>For further information on how to become involved please contact Chris on 0406 992 766.</P>
    </div>
  );
}

export default Info;