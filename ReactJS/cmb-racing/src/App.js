import React, {Suspense} from 'react';
import './App.css';
import 'bootstrap/dist/css/bootstrap.min.css';
import "slick-carousel/slick/slick.css"; 
import "slick-carousel/slick/slick-theme.css";
import { BrowserRouter, Route, useLocation, useHistory, useRouteMatch } from "react-router-dom";

// import Header from "./components/Header";
// import HomePage from "./containers/HomePage";
// import About from "./containers/About";
// import Contact from "./containers/Contact";
const Header = React.lazy(() => import('./components/Header'));
const HomePage = React.lazy(() => import('./containers/HomePage'));
const About = React.lazy(() => import('./containers/About'));
const Contact = React.lazy(() => import('./containers/Contact'));

function ArtistComponent() {
  return (
    <div className="event-list">
      
    </div>
  );
}

function App() {
  return (
    <div className="App">
      <BrowserRouter>
        <Suspense fallback={<div>Loading...</div>}>
          <Header />
        </Suspense>
        <Suspense fallback={<div>Loading...</div>}>
          <Route exact path="/" component={HomePage} />
        </Suspense>
        <Suspense fallback={<div>Loading...</div>}>
          <Route exact path="/about" >
            <About />
          </Route>
        </Suspense>
        <Suspense fallback={<div>Loading...</div>}>
          <Route exact path="/contact" component={Contact} />
        </Suspense>
      </BrowserRouter>
    </div>
  );
}

export default App;